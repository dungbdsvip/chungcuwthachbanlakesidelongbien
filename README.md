# chungcuwthachbanlakesidelongbien

Thạch Bàn Lakeside Long Biên dự án chung cư cao cấp tại Long Biên, Hà Nội.  Chung cư Lakeside Thạch Bàn Tọa lạc tại vị trí đắc địa của quận Long Biên, cùng với không gian thoáng mát,

<strong><span style="color: #003366;"><a style="color: #003366;" href="http://kenhbatdongsanviet.com/thach-ban-lakeside-long-bien"><strong>Thạch Bàn Lakeside</strong></a> </span>Long Biên dự án chung cư cao cấp tại Long Biên, Hà Nội. <span style="color: #003366;"> <a style="color: #003366;" href="http://kenhbatdongsanviet.com/thach-ban-lakeside-long-bien"><strong>Chung cư Lakeside Thạch Bàn</strong></a></span> Tọa lạc tại vị trí đắc địa của quận Long Biên, cùng với không gian thoáng mát, rộng lấy khí trời từ không gian hồ Thạch Bàn rộng lớn</strong>.

[caption id="attachment_3129" align="aligncenter" width="640"]<a href="http://kenhbatdongsanviet.com/wp-content/uploads/2018/02/Tổng-quan-dự-án-Thạch-Bàn-Lakeside.jpg"><img class="Tổng quan dự án Thạch Bàn Lakeside wp-image-3129 size-full" title="Tổng quan dự án Thạch Bàn Lakeside" src="http://kenhbatdongsanviet.com/wp-content/uploads/2018/02/Tổng-quan-dự-án-Thạch-Bàn-Lakeside.jpg" alt="Tổng quan dự án Thạch Bàn Lakeside" width="640" height="466" /></a> Tổng quan dự án Thạch Bàn Lakeside[/caption]
<h2>Tổng quan dự án chung cư Thạch Bàn Lakeside</h2>
<strong>Tên dự án: <span style="color: #003366;"><a style="color: #003366;" href="http://kenhbatdongsanviet.com/thach-ban-lakeside-long-bien"><strong>Thạch Bàn Lakeside</strong></a></span></strong>
Chủ đầu tư: Công ty TNHH Tập đoàn MIK GROUP(MIK GROUP)
Vị trí : Tổ 4, phường Thạch Bàn, quận Long Biên, Thành phố Hà Nội
Tổng diện tích dự án: 72.481 m2, trong đó:
+ Diện tích đường giao thông: 8.233 m2
+ Diện tích dự án: 64.248 m2
Loại hình phát triển: căn hộ cao cấp
Tổng số căn giai đoạn 1: hơn 700 căn

<strong>Quy mô <a href="http://kenhbatdongsanviet.com/thach-ban-lakeside-long-bien"><span style="color: #003366;"><strong>dự án Lakeside Thạch Bàn</strong></span></a><span style="color: #003366;">:</span></strong>
- Dịch vụ phục vụ cộng đồng gồm: trung tâm thương mại; văn phòng cho thuê; nhà trẻ (bố trí cho khoảng 150- 200 cháu); công trình dịch vụ khác (TDTT, ăn uống, vui chơi giải trí…)
- Nhà ở <span style="color: #003366;"><a style="color: #003366;" href="http://kenhbatdongsanviet.com/thach-ban-lakeside-long-bien"><strong>Thạch Bàn Lakeside</strong></a></span> gồm các loại hình: chung cư cao tầng- Chiều cao các tòa tháp: tối đa 100m (25 đến 28 tầng).
- Trục đường chính: rộng 11,5m (3 làn xe), vỉa hè 2 bên rộng 5m/bên.
- Đường nội bộ giữa các khu nhà: rộng 7,5m (2 làn xe), vỉa hè 2 bên rộng 3m/bên.
Kết cấu: 3 tầng hầm, 3 tầng TTTM (nối liền 3 toà nhà)
Tầng 1: Siêu thị, nhà sinh hoạt cộng đồng, sảnh lễ tân
Căn hộ diện tích: Từ 80m2 đến 130m2.

Đơn vị thiết kếcủa <strong><a href="http://kenhbatdongsanviet.com/thach-ban-lakeside-long-bien"><span style="color: #003366;"><strong>Chung cư Lakeside Thạch Bàn</strong></span></a> </strong>: AEDAS VIET NAM, ADCI, ATEK
Tư vấn và giám sát: Công ty CP Texo tư vấn và đầu tư
Đơn thị thi công: Tổng cty đầu tư phát triển đô thị hạ tầng UDIC và cty TNHH MTV xí nghiệp xây dựng số 5
Giá bán căn hộ: từ 20 triệu/m2
Thời gian bàn giao: Quý II/2019
<h2>Vị trí chung cư Thạch Bàn Lakeside</h2>
[caption id="attachment_3131" align="aligncenter" width="1024"]<a href="http://kenhbatdongsanviet.com/wp-content/uploads/2018/02/Vị-tri-chung-cư-Thạch-Bàn-Lakeside.jpg"><img class="Vị trí chung cư Thạch Bàn Lakeside wp-image-3131 size-large" title="Vị trí chung cư Thạch Bàn Lakeside" src="http://kenhbatdongsanviet.com/wp-content/uploads/2018/02/Vị-tri-chung-cư-Thạch-Bàn-Lakeside-1024x467.jpg" alt="Vị trí chung cư Thạch Bàn Lakeside" width="1024" height="467" /></a> Vị trí chung cư Thạch Bàn Lakeside[/caption]

<a href="http://kenhbatdongsanviet.com/thach-ban-lakeside-long-bien"><span style="color: #003366;"><strong>Dự án Thạch Bàn Lakeside</strong></span></a> có vị trí thuận lợi

Chỉ mất mười phút đường bộ để tới trung tâm Thành phố Hà Nội

Dễ dàng tiếp cận với các tuyến đường chính của Thành phố Hà Nội như Quốc lộ 5 (Hà Nội - Hải Phòng)

Đường vành đai 3 (qua cầu Thanh Trì), Đường vành đai 2 (qua cầu Vĩnh Tuy).

Là khu vực phát triển đô thị tập trung xây mới tại Thủ đô theo quy hoạch chung xây dựng Thủ đô Hà Nội được Thủ tướng Chính Phủ phê duyệt và thuộc phạm vi điều chỉnh Quy hoạch chi tiết Quận Long Biên tỉ lệ 1/2000, khu vực Tây Nam Quận Long Biên đã được phê duyệt.

Địa hình bằng phẳng, có hạ tầng tương đối hoàn chỉnh gồm đường giao thông bao quanh, vỉa hè, giao thông nội bộ, sân bãi, các điểm đấu nối điện, cấp thoát nước…

Phía Tây Bắc giáp đường Thạch Bàn hiện có, nút giao cắt của đường Thạch Bàn và đường quy hoạch dự kiến;

Phía Bắc giáp đường quy hoạch dự kiến (có mặt cắt ngang 13,5m) phân cách với Khu Công viên sinh thái;

Về Phía Nam và Đông Nam giáp đường quy hoạch dự kiến phân cách với kênh và hồ nước;

Phía Đông giáp giáp đường quy hoạch dự kiến phân cách với kênh và hồ nước

Phía Tây và Tây Nam giáp khu dân cư hiện có thuộc Phường Thạch Bàn và khu tái định cư dự kiến phục vụ dự án Công viên Công nghệ Thông tin
<h2>Tiện ích chung cư Thạch Bàn Lakeside</h2>
[caption id="attachment_3132" align="aligncenter" width="667"]<a href="http://kenhbatdongsanviet.com/wp-content/uploads/2018/02/Thạch-Bàn-Lakeside-Tiện-ích-khu-vực.jpg"><img class="Thạch Bàn Lakeside - Tiện ích khu vực wp-image-3132 size-full" title="Thạch Bàn Lakeside - Tiện ích khu vực" src="http://kenhbatdongsanviet.com/wp-content/uploads/2018/02/Thạch-Bàn-Lakeside-Tiện-ích-khu-vực.jpg" alt="Thạch Bàn Lakeside - Tiện ích khu vực" width="667" height="468" /></a> Thạch Bàn Lakeside - Tiện ích khu vực[/caption]

[caption id="attachment_3133" align="aligncenter" width="1325"]<a href="http://kenhbatdongsanviet.com/wp-content/uploads/2018/02/Tiện-ích-nội-khu-Thạch-Bàn-Lakeside.jpg"><img class="Tiện ích nội khu - Thạch Bàn Lakeside wp-image-3133 size-full" title="Tiện ích nội khu - Thạch Bàn Lakeside" src="http://kenhbatdongsanviet.com/wp-content/uploads/2018/02/Tiện-ích-nội-khu-Thạch-Bàn-Lakeside.jpg" alt="Tiện ích nội khu - Thạch Bàn Lakeside" width="1325" height="659" /></a> Tiện ích nội khu - Thạch Bàn Lakeside[/caption]
<h2>Thiết kế mặt bằng tổng thể dự án Thạch Bàn Lakeside</h2>
[caption id="attachment_3134" align="aligncenter" width="1600"]<a href="http://kenhbatdongsanviet.com/wp-content/uploads/2018/02/Mặt-bằng-tổng-thể-chung-cư-Thạch-Bàn-Lakeside.jpg"><img class="Mặt bằng tổng thể chung cư Thạch Bàn Lakeside wp-image-3134 size-full" title="Mặt bằng tổng thể chung cư Thạch Bàn Lakeside" src="http://kenhbatdongsanviet.com/wp-content/uploads/2018/02/Mặt-bằng-tổng-thể-chung-cư-Thạch-Bàn-Lakeside.jpg" alt="Mặt bằng tổng thể chung cư Thạch Bàn Lakeside" width="1600" height="1100" /></a> Mặt bằng tổng thể chung cư Thạch Bàn Lakeside[/caption]

<span style="color: #003366;"><strong>Quản lý <a style="color: #003366;" href="http://kenhbatdongsanviet.com/thach-ban-lakeside-long-bien"><strong>dự án chung cư Thạch Bàn Lakeside Long Biên</strong></a></strong></span>

<span style="color: #003366;"><strong>Hotline 1: <a style="color: #003366;" href="tel:0968699754">0968 699 754</a></strong></span>

<span style="color: #003366;"><strong>Hotline 2: <a style="color: #003366;" href="tel:0969292196">096 9292 196</a></strong></span>


Nguồn bài viết:  http://kenhbatdongsanviet.com/thach-ban-lakeside-long-bien 